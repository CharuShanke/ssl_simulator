
package com.lentra.simulator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Micro Service Runner
 *
 * @author dimit
 */
@SpringBootApplication
public class SimulatorRunner {

    public static void main(String[] args)
    {
        SpringApplication.run(SimulatorRunner.class, args);
        System.out.println("Microservice Simulator Has Started...");
    }

}
