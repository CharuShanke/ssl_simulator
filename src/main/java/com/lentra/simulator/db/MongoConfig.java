/*
 * Copyright (c) RX Technologies
 * Leading Healthcare Service Provider
 * All rights reserved.
 *
 * No parts of this source code can be reproduced without written
 * consent from RX Technologies.
 * www.rx.com
 *
 */

package com.lentra.simulator.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

/**
 *
 * @author dimit
 * @date 11-Mar-2022
 */
@Configuration
public class MongoConfig {

    @Value ("${spring.data.mongodb.url}")
    private String dbUrl;

    @Autowired
    private MongoDatabaseFactory mongoDbFactory;

    public @Bean
    MongoDatabaseFactory mongoDBFactory() throws Exception
    {
        return new SimpleMongoClientDatabaseFactory(dbUrl);
    }

    public @Bean
    MongoTemplate mongoTemplate() throws Exception
    {

        DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory);

        // Remove _class
        MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, new MongoMappingContext());
        converter.setTypeMapper(new DefaultMongoTypeMapper(null));

        return new MongoTemplate(mongoDBFactory(), converter);

    }

}
