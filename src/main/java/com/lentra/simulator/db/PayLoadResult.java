/*
 * Copyright (c) RX Technologies
 * Leading Healthcare Service Provider
 * All rights reserved.
 *
 * No parts of this source code can be reproduced without written
 * consent from RX Technologies.
 * www.rx.com
 *
 */

package com.lentra.simulator.db;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author dimit
 * @date 11-Mar-2022
 */
@Data
@AllArgsConstructor
public class PayLoadResult {
    
    private String message;

}
