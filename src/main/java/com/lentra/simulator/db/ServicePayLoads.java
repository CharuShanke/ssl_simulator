
package com.lentra.simulator.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author dimit
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document (collection = "service_pay_loads")
public class ServicePayLoads {

//    @Id
//    private String id;
    private String serviceType;
    private Object payLoad;
    private String mobileNumber;
    private String panNumber;

}
