package com.lentra.simulator.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author dimit
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SSLRequest {

    @JsonProperty("sRequestjson")
    private String requestjson;

    @JsonProperty("sRequestType")
    private String requestType;

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("sInstitutionName")
    private String institutionName;

    @JsonProperty("sTransactionId")
    private String transactionId;

    @JsonProperty("sAcknowledgementId")
    private String acknowledgementId;

}
