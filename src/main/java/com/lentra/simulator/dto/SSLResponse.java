
package com.lentra.simulator.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author dimit
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SSLResponse {

    private String acknowledgementId;

    private String transactionId;

    private String path;

    private Date timeStamp;

    @JsonInclude (JsonInclude.Include.NON_NULL)
    private int statusCode;

    @JsonInclude (JsonInclude.Include.NON_NULL)
    private String errorMessage;

    @JsonInclude (JsonInclude.Include.NON_NULL)
    private Object payload;

}
