/*
 * Copyright (c) RX Technologies
 * Leading Healthcare Service Provider
 * All rights reserved.
 *
 * No parts of this source code can be reproduced without written
 * consent from RX Technologies.
 * www.rx.com
 *
 */

package com.lentra.simulator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author dimit
 * @date 11-Mar-2022
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceFilter {
    
    private String serviceType;
    private String mobileNumber;
    private String panNumber;
            

}
