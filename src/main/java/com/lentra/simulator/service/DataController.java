
package com.lentra.simulator.service;

import com.lentra.simulator.db.PayLoadResult;
import com.lentra.simulator.db.ServicePayLoads;
import com.lentra.simulator.dto.ServiceFilter;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dimit
 */
@RestController
@RequestMapping (value = "${spring.application.name}/v1/")
public class DataController {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     *
     * @param sslRequest
     *
     * @return
     *
     * @throws Exception
     */
    @GetMapping (path = "/ping")
    public String ping()
    {
        return new String("I Am ALIVE ! Play With Me !!");
    }

    /**
     *
     * @param sslRequest
     *
     * @return
     *
     * @throws Exception
     */
    @PostMapping (path = "/push", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PayLoadResult push(@RequestBody ServicePayLoads servicePayLoads)
    {
        mongoTemplate.save(servicePayLoads);
        return new PayLoadResult("Use Case Data Push Successfully!");
    }

    /**
     * Get Data
     *
     * @param serviceFilter
     *
     * @return
     */
    @PostMapping (path = "/getData", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ServicePayLoads>> getByAttribute(@RequestBody ServiceFilter serviceFilter)
    {
        try
        {
            String serviceType = serviceFilter.getServiceType();
            String mobileNumber = serviceFilter.getMobileNumber();
            String panNumber = serviceFilter.getPanNumber();
            Query query = null;

            List<ServicePayLoads> servicePayLoads = new ArrayList<ServicePayLoads>();

            if ( ! isValid(serviceType) &&  ! isValid(mobileNumber) &&  ! isValid(panNumber))
            {
                servicePayLoads = mongoTemplate.findAll(ServicePayLoads.class);
            } else
            {
                if (isValid(serviceType))
                {
                    query = Query.query(Criteria.where("serviceType").is(serviceType));

                    if (isValid(mobileNumber))
                    {
                        query.addCriteria(Criteria.where("mobileNumber").is(mobileNumber));
                    } else if (isValid(panNumber))
                    {
                        query.addCriteria(Criteria.where("panNumber").is(panNumber));
                    }
                } else if (isValid(panNumber))
                {
                    query = Query.query(Criteria.where("panNumber").is(panNumber));
                } else if (isValid(mobileNumber))
                {
                    query = Query.query(Criteria.where("mobileNumber").is(mobileNumber));
                }

                servicePayLoads = mongoTemplate.find(query, ServicePayLoads.class);
            }
            if (servicePayLoads.isEmpty())
            {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(servicePayLoads, HttpStatus.OK);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private boolean isValid(String value)
    {
        return  ! "".equals(value) &&  ! value.isEmpty() &&  ! "null".equals(value);
    }
}
