
package com.lentra.simulator.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lentra.simulator.db.ServicePayLoads;
import com.lentra.simulator.dto.SSLRequest;
import com.lentra.simulator.dto.SSLResponse;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author dimit
 */
@RestController
@RequestMapping (value = "${spring.application.name}/v1/")
public class SimulatorController {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ObjectMapper mapper;

    /**
     *
     * @param sslRequest
     *
     * @return
     *
     * @throws Exception
     */
    @PostMapping (path = "/simulate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SSLResponse simulate(@RequestBody SSLRequest sslRequest) throws Exception
    {

        SSLResponse sslResponse = new SSLResponse();
        String ackId = UUID.randomUUID().toString().replace("-", "").substring(0, 23);
        sslResponse.setAcknowledgementId(ackId);
        sslResponse.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
        sslResponse.setTimeStamp(new Date());
        sslResponse.setPath("/hdfc/encryptAndSend");
        sslResponse.setStatusCode(200);

        String request = sslRequest.getRequestjson();

        Document doc = null;
        Map<String, String> requestAttributes = new HashMap<>();
        if (isXML(request))
        {
            doc = convertXmlToDocument(request);
        } else
        {
            Map<String, Object> requestMap = mapper.readValue(request, new TypeReference<Map<String, Object>>() {
            });
            String key = "";
            for (Map.Entry<String, Object> entry : requestMap.entrySet())
            {
                key = entry.getKey();
            }
            requestAttributes = (Map<String, String>) requestMap.get(key);
        }

        String serviceType = sslRequest.getRequestType();

        Query query = Query.query(Criteria.where("serviceType").is(serviceType));

        String mobileNumber = "";
        String panNumber = "";
        switch (serviceType)
        {
            case "GENERATE_OTP":
                mobileNumber = String.valueOf(requestAttributes.get("mobilenumber"));
                if (mobileNumber != null &&  ! "null".equals(mobileNumber))
                {
                    query.addCriteria(Criteria.where("mobileNumber").is(mobileNumber));
                }
                break;
            case "CHECK_OFFER_V2":
            case "CIF_LOS_V2":
                mobileNumber = String.valueOf(requestAttributes.get("mobilenumber"));
                if (mobileNumber != null &&  ! "null".equals(mobileNumber))
                {
                    query.addCriteria(Criteria.where("mobileNumber").is(mobileNumber));
                }
                panNumber = String.valueOf(requestAttributes.get("panNumber"));
                if (panNumber != null &&  ! "null".equals(panNumber))
                {
                    query.addCriteria(Criteria.where("panNumber").is(panNumber));
                }
                break;
            case "PAN_VALIDATION":
                panNumber = doc.getElementsByTagName("PAN").item(0).getTextContent();
                if (panNumber != null &&  ! "null".equals(panNumber))
                {
                    query.addCriteria(Criteria.where("panNumber").is(panNumber));
                }
                break;

            default:
                break;
        }

        ServicePayLoads servicePayLoads = mongoTemplate.findOne(query, ServicePayLoads.class);

        if (servicePayLoads != null)
        {
            Object payLoad = servicePayLoads.getPayLoad();
            sslResponse.setPayload(payLoad);
        } else
        {
            System.out.println("Service Type Not Found");
        }
        return sslResponse;
    }

    /**
     *
     * @param xml
     *
     * @return
     */
    private Document convertXmlToDocument(String xml)
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder;
        try
        {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xml)));
            return doc;
        }
        catch (IOException | ParserConfigurationException | SAXException e)
        {
            System.out.println("Problem in Creating Document from XML  --> " + e.getMessage());
        }
        return null;
    }

    /**
     *
     * @param value
     *
     * @return
     */
    private boolean isXML(String value)
    {
        boolean flag = true;
        try
        {
            DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(value)));
        }
        catch (ParserConfigurationException | SAXException | IOException ex)
        {
            flag = false;
        }
        return flag;
    }

}
